package main.java;

import java.util.Scanner;

public class ExercicioSete {

    /*
    public static void main(String[] args) {

        // Custos de pintura de um edifício.
        // Estrutura de dados
        double area, custoTintaLitro, custoTinta, rendimentoLitro, salarioPintorDia, custoPintor, custoTotal, numeroPintores;

        // Leitura de dados
        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza a área a ser pintada em m2");
        area = ler.nextDouble();
        System.out.println("Introduza o custo de um litro de tinta");
        custoTintaLitro = ler.nextDouble();
        System.out.println("Introduza o rendimento da tinta (quantos litros por m2)");
        rendimentoLitro = ler.nextDouble();
        System.out.println("Introduza o salário por dia de um pintor");
        salarioPintorDia = ler.nextDouble();

        // Processamento

        numeroPintores = getNumeroPintores(area);

        if ( numeroPintores == 0 ) {
            System.out.println("Valor inválido");

        } else {
            custoTinta = getCustoTinta(area, custoTintaLitro, rendimentoLitro);
            custoPintor = getCustoPintor(area, salarioPintorDia, numeroPintores);
            custoTotal = getCustoTotal(custoTinta, custoPintor);

            System.out.println("O custo da tinta é " + String.format("%.2f", custoTinta) + ", o custo da mão-de-obra é " + String.format("%.2f", custoPintor) + ", o custo total é " + String.format("%.2f", custoTotal));

        }

    }

     */

    public static double getNumeroPintores(double area) {

        int numeroPintores;

        if (area > 0 && area < 100) {
            numeroPintores = 1;
        } else if (area >= 100 && area < 300) {
            numeroPintores = 2;
        } else if (area >= 300 && area < 1000) {
            numeroPintores = 3;
        } else if (area >= 1000){
            numeroPintores = 4;
        } else {
            numeroPintores = 0;
        }

        return numeroPintores;
    }

    public static double getCustoTinta(double area, double custoTintaLitro, double rendimentoLitro) {
        return Math.ceil(area / rendimentoLitro) * custoTintaLitro;
    }

    public static double getCustoPintor(double area, double salarioPintorDia, double numeroPintores) {
        return Math.ceil((area / (numeroPintores * 2)) / 8) * salarioPintorDia;
    }

    public static double getCustoTotal(double custoTinta, double custoPintor) {
        return custoTinta + custoPintor;
    }

}
