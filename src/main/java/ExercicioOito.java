package main.java;

import java.util.Scanner;

public class ExercicioOito {

    /*
    public static void main(String[] args) {

        // Estabelecer se um número é múlitplo de outro.
        // Estrutura de dados
        int numero1, numero2;

        // Leitura de dados
        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o primeiro número");
        numero1 = ler.nextInt();
        System.out.println("Introduza o segundo número");
        numero2 = ler.nextInt();

        // Processamento e escritura de dados
        System.out.println( isMultiplo(numero1, numero2) );

    }

     */

    public static String isMultiplo(int numero1, int numero2) {

        String result;

        if (numero1 % numero2 == 0) {
            result = (numero1 + " é múltiplo de " + numero2);
        } else if (numero2 % numero1 == 0) {
            result = (numero2 + " é múltiplo de " + numero1);
        } else {
            result = (numero1 + " não é múltiplo nem divisor de " + numero2);
        }

        return result;
    }

}
