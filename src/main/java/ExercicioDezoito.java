package main.java;

public class ExercicioDezoito {

    /*
    public static void main(String[] args) {

        // Determinar hora de fim de processamento da tarefa duma máquina através dos segundos da sua duração
        // Estrutura de dados
        int horasInicio, minutosInicio, segundosInicio, segundosDuracao, totalSegundosInicio, horasFim, minutosFim, segundosFim;

        // Leitura de dados
        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza a hora de início");
        horasInicio = ler.nextInt();
        System.out.println("Introduza os minutos de início");
        minutosInicio = ler.nextInt();
        System.out.println("Introduza os segundos de início");
        segundosInicio = ler.nextInt();
        System.out.println("Introduza os segundos da duração da tarefa");
        segundosDuracao = ler.nextInt();

        // Processamento
        if ( !isValido(segundosDuracao) ) {
            System.out.println("Valor inválido");
        } else {
            totalSegundosInicio = getTotalSegundosInicio(horasInicio, minutosInicio, segundosInicio);
            horasFim = getHorasFim(totalSegundosInicio, segundosDuracao);
            minutosFim = getMinutosFim(totalSegundosInicio, segundosDuracao);
            segundosFim = getSegundosFim(totalSegundosInicio, segundosDuracao);

            System.out.println("A tarefa termina às " + horasFim + ":" + minutosFim + ":" + segundosFim );
        }


    }

     */

    public static boolean isValido(int segundosDuracao) {
        return segundosDuracao > 0;
    }

    public static int getTotalSegundosInicio(int horasInicio, int minutosInicio, int segundosInicio) {
        return horasInicio * 3600 + minutosInicio * 60 + segundosInicio;
    }

    public static int getHorasFim(int totalSegundosInicio, int segundosDuracao) {
        int result;

        result = (totalSegundosInicio + segundosDuracao) / 3600;

        if (result >= 24) {
            result = result - 24;
        }

        return result;
    }

    public static int getMinutosFim(int totalSegundosInicio, int segundosDuracao) {
        return ((totalSegundosInicio + segundosDuracao) % 3600) / 60;
    }

    public static int getSegundosFim(int totalSegundosInicio, int segundosDuracao) {
        return ((totalSegundosInicio + segundosDuracao) % 3600) % 60;
    }
}