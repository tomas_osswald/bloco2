package main.java;

import java.util.Scanner;

public class ExercicioDois {

    /*
    public static void main(String[] args) {

        // Função para separar os 3 dígitos de um número em inteiros.
        // Estrutura de dados
        int numero, digito1, digito2,digito3;

        // Leitura de dados
        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza um número com 3 dígitos");
        numero = ler.nextInt();

        // Verificação - 'numero' tem 3 dígitos
        if ( numero < 100 || numero > 999) {
            System.out.println("Número não tem 3 dígitos");

        } else {

            // Processamento
            digito3 = getDigito3(numero);
            digito2 = getDigito2(numero);
            digito1 = getDigito1(numero);

            System.out.println(digito1 + "" + digito2 + "" + digito3);

            if (isParOuImpar(digito3)) {
                System.out.println("O número é impar");
            } else {
                System.out.println("O número é par");
            }
        }

    } */

    public static int getDigito3(int numero) {
        return numero % 10;
    }

    public static int getDigito2(int numero) {
        return (numero / 10) % 10;
    }

    public static int getDigito1(int numero) {
        return (numero / 100) % 10;
    }

    public static boolean isParOuImpar(int digito3) {

        return digito3 % 2 == 1;
    }

}
