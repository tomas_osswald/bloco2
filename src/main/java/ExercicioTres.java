package main.java;

import java.util.Scanner;

public class ExercicioTres {

    /*
    public static void main(String[] args) {

        // Função para calcular a distância entre dois pontos num plano.
        // Estrutura de dados
        int xPonto1, yPonto1, xPonto2, yPonto2;
        double distancia;

        // Leitura de dados
        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o valor x do Ponto 1");
        xPonto1 = ler.nextInt();
        System.out.println("Introduza o valor y do Ponto 1");
        yPonto1 = ler.nextInt();
        System.out.println("Introduza o valor x do Ponto 2");
        xPonto2 = ler.nextInt();
        System.out.println("Introduza o valor y do Ponto 2");
        yPonto2 = ler.nextInt();

        // Processamento
        distancia = getDistancia(xPonto1, yPonto1, xPonto2, yPonto2);

        // Escritura de dados
        System.out.println("A distância entre os dois pontos é " + String.format("%.2f", distancia));
    }
     */

    public static double getDistancia(int xPonto1, int yPonto1, int xPonto2, int yPonto2) {
        return Math.sqrt((Math.pow(xPonto2 - xPonto1, 2)) + (Math.pow(yPonto2 - yPonto1, 2)));
    }

}
