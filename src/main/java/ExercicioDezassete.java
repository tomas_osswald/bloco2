package main.java;

import java.util.Scanner;

public class ExercicioDezassete {

    /*
    public static void main(String[] args) {

        // Determinar hora de chegada de um comboio através de hora de partida e duração da viagem
        // Estrutura de dados
        int horasPartida, minutosPartida, horasDuracao, minutosDuracao, horasChegada, minutosChegada;

        // Leitura de dados
        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza a hora da partida");
        horasPartida = ler.nextInt();
        System.out.println("Introduza os minutos da partida");
        minutosPartida = ler.nextInt();
        System.out.println("Introduza a hora da duração da viagem");
        horasDuracao = ler.nextInt();
        System.out.println("Introduza os minutos da duração da viagem");
        minutosDuracao = ler.nextInt();

        // Processamento
        if ( !isValido(horasDuracao, minutosDuracao) ) {
            System.out.println("Valor inválido");
        } else {
            horasChegada = getHorasChegada(horasPartida, minutosPartida, horasDuracao, minutosDuracao);
            minutosChegada = getMinutosChegada(horasPartida, minutosPartida, horasDuracao, minutosDuracao);

            System.out.println("O comboio chega às " + horasChegada + ":" + minutosChegada );
            System.out.println(getDiaChegada(horasPartida, minutosPartida, horasDuracao, minutosDuracao));
        }


    }

     */

    public static boolean isValido(int horasDuracao, int minutosDuracao) {
        return (horasDuracao > 0 && horasDuracao < 24) || (horasDuracao == 0 && minutosDuracao > 0) || (horasDuracao == 24 && minutosDuracao == 0);
    }

    public static int getHorasChegada(int horasPartida, int minutosPartida, int horasDuracao, int minutosDuracao) {
        int result;

        result = ( (horasPartida * 60 + minutosPartida) + (horasDuracao * 60 + minutosDuracao) ) / 60;

        if (result >= 24) {
            result = result - 24;
        }

        return result;
    }

    public static int getMinutosChegada(int horasPartida, int minutosPartida, int horasDuracao, int minutosDuracao) {
        return ( (horasPartida * 60 + minutosPartida) + (horasDuracao * 60 + minutosDuracao) ) % 60;
    }

    public static String getDiaChegada(int horasPartida, int minutosPartida, int horasDuracao, int minutosDuracao) {
        String result;
        int horasChegada;

        horasChegada = ( (horasPartida * 60 + minutosPartida) + (horasDuracao * 60 + minutosDuracao) ) / 60;

        if (horasChegada >= 24) {
            result = "O comboio chega no dia seguinte ao de partida";
        } else {
            result = "O comboio chega no mesmo dia em que parte";
        }

        return result;
    }
}
