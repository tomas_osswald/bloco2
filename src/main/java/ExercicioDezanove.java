package main.java;

import java.util.Scanner;

public class ExercicioDezanove {

    /*
    public static void main(String[] args) {

        // Calcular salário semanal de um empregado tendo em conta horas extra
        // Estrutura de dados
        double horasTrabalho, salarioSemanal;

        // Leitura de dados
        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o número de horas de trabalho da semana");
        horasTrabalho = ler.nextDouble();

        // Processamento
        if ( !isValido(horasTrabalho) ) {
            System.out.println("Valor inválido");

        } else {
            salarioSemanal = getSalarioSemanal(horasTrabalho);
            System.out.println("O salário semanal do empregado é " + String.format("%.2f", salarioSemanal) );
        }
    }

     */

    public static boolean isValido(double horasTrabalho) {
        return horasTrabalho > 0;
    }

    public static double getSalarioSemanal(double horasTrabalho) {
        double salarioSemanal;
        double salarioHora = 7.5;
        double salarioHoraExtraUm = 10;
        double salarioHoraExtraDois = 15;

        if (horasTrabalho <= 36) {
            salarioSemanal = horasTrabalho * salarioHora;
        } else if (horasTrabalho > 36 && horasTrabalho <= 41) {
            salarioSemanal = ( (horasTrabalho - 36) * salarioHoraExtraUm) + (36 * salarioHora);
        } else {
            salarioSemanal = ( (horasTrabalho - 41) * salarioHoraExtraDois) + (5 * salarioHoraExtraUm) + (36 * salarioHora);
        }

        return salarioSemanal;
    }
}
