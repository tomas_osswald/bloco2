package main.java;

import java.util.Scanner;

public class ExercicioQuatro {

    /*
    public static void main(String[] args) {

        // Cálcular os valores da função.
        // Estrutura de dados
        double variavel, resultado;

        // Leitura de dados
        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o valor da variável");
        variavel = ler.nextDouble();

        // Processamento

        resultado = getResultado(variavel);

        // Escritura de dados
        System.out.println("O valor da função é " + String.format("%.2f", resultado));

    }

     */

    public static double getResultado(double variavel) {
        if (variavel < 0) {
            return variavel;
        } else if (variavel == 0) {
            return 0;
        } else {
            return Math.pow(variavel, 2) - (2 * variavel);
        }
    }
}
