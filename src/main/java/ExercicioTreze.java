package main.java;

import java.util.Scanner;

public class ExercicioTreze {

    /*
    public static void main(String[] args) {

        // Funçao para cálculo de orçamento de construção de jardim
        // Estrutura de dados
        double areaGrama, arvores, arbustos, horasTotal, custoTotal;
        double custoGrama = 10;
        double custoArvores = 20;
        double custoArbustos = 15;
        double tempoGrama = 300;
        double tempoArvores = 600;
        double tempoArbustos = 400;
        double custoTrabalhoHora = 10;

        // Leitura de dados
        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza a área de grama");
        areaGrama = ler.nextDouble();
        System.out.println("Introduza o número de árvores a plantar");
        arvores = ler.nextDouble();
        System.out.println("Introduza o número de arbustos a plantar");
        arbustos = ler.nextDouble();

        // Processamento
        if (isInvalido(areaGrama, arvores, arbustos)) {
            System.out.println("Valores inválidos");
        } else {

            horasTotal = getHorasTotal(areaGrama, arvores, arbustos, tempoGrama, tempoArvores, tempoArbustos);
            custoTotal = getCustoTotal(areaGrama, arvores, arbustos, horasTotal, custoGrama, custoArvores, custoArbustos, custoTrabalhoHora);

            // Escritura de dados
            System.out.println("O custo da construção deste jardim será " + String.format("%.2f", custoTotal) + " Euros e serão necessárias " + String.format("%.0f", horasTotal) + " horas de trabalho.");
        }
    }

     */

    public static boolean isInvalido(double areaGrama, double arvores, double arbustos) {
        return areaGrama < 0 || arvores < 0 || arbustos < 0;
    }

    public static double getHorasTotal(double areaGrama, double arvores, double arbustos, double tempoGrama, double tempoArvores, double tempoArbustos) {
        return Math.ceil(((areaGrama * tempoGrama) + (arvores * tempoArvores) + (arbustos * tempoArbustos)) / 3600);
    }

    public static double getCustoTotal(double areaGrama, double arvores, double arbustos, double horasTotal, double custoGrama, double custoArvores, double custoArbustos, double custoTrabalhoHora) {
        return (areaGrama * custoGrama) + (arvores * custoArvores) + (arbustos * custoArbustos) + (custoTrabalhoHora * horasTotal);
    }
}
