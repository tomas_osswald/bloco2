package main.java;

import java.util.Scanner;

public class ExercicioQuinze {

    /*
    public static void main(String[] args) {

        // Classificar um triângulo e verificar se ele é possível através dos seus lados
        // Estrutura de dados
        double ladoA, ladoB, ladoC;

        // Leitura de dados
        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o valor do lado A do triângulo");
        ladoA = ler.nextDouble();
        System.out.println("Introduza o valor do lado B do triângulo");
        ladoB = ler.nextDouble();
        System.out.println("Introduza o valor do lado C do triângulo");
        ladoC = ler.nextDouble();

        // Processamento
        if ( isTrianguloImpossivel(ladoA, ladoB, ladoC) ) {
            System.out.println("Triângulo não é possível");
        } else {
            if ( isTrianguloEquilatero (ladoA, ladoB, ladoC) ) {
                System.out.println("Triângulo é equilátero");
            } else if ( isTrianguloEscaleno (ladoA, ladoB, ladoC) ) {
                System.out.println("Triângulo é escaleno");
            } else {
                System.out.println("Triângulo é isósceles");
            }
        }

    }

     */

    public static boolean isTrianguloImpossivel(double ladoA, double ladoB, double ladoC) {
        return ladoA <= 0 || ladoB <= 0 || ladoC <= 0 || ladoA > ladoB + ladoC || ladoB > ladoA + ladoC || ladoC > ladoA + ladoB;
    }

    public static boolean isTrianguloEquilatero(double ladoA, double ladoB, double ladoC) {
        return ladoA == ladoB && ladoB == ladoC;
    }

    public static boolean isTrianguloEscaleno(double ladoA, double ladoB, double ladoC) {
        return ladoA != ladoB && ladoB != ladoC;
    }

}
