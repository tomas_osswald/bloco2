package main.java;

import java.util.Scanner;

public class ExercicioOnze {

    /*
    public static void main(String[] args) {

        // Função para classificar a percentagem de aprovação da turma
        // Estrutura de dados
        double aprovados, limiteMin, limiteMax;

        // Leitura de dados
        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o limite de validação mínimo");
        limiteMin = ler.nextDouble();
        System.out.println("Introduza o limite de validação máximo");
        limiteMax = ler.nextDouble();
        System.out.println("Introduza a percentagem de aprovados");
        aprovados = ler.nextDouble();

        //Processamento
        System.out.println(getClassificacao(aprovados, limiteMin, limiteMax));

    }

     */

    public static String getClassificacao(double aprovados, double limiteMin, double limiteMax) {

        String result;

        if ( aprovados < limiteMin || aprovados > limiteMax) {
            result = "Valor inválido";
        } else if ( aprovados < 0.2 ) {
            result = "Turma má";
        } else if ( aprovados < 0.5 ) {
            result = "Turma fraca";
        } else if ( aprovados < 0.7 ) {
            result = "Turma razoável";
        } else if ( aprovados < 0.9 ) {
            result = "Turma boa";
        } else {
            result = "Turma excelente";
        }

        return result;
    }

}
