package main.java;

import org.junit.Test;

import static org.junit.Assert.*;

public class ExercicioSeteTeste {

    @Test
    public void getNumeroPintoresTesteUm() {
        // arrange
        double area = 0;
        double expected = 0;

        // act
        double result = ExercicioSete.getNumeroPintores(area);

        // assert
        assertEquals(result, expected, 0.01);
    }

    @Test
    public void getNumeroPintoresTesteDois() {
        // arrange
        double area = -1;
        double expected = 0;

        // act
        double result = ExercicioSete.getNumeroPintores(area);

        // assert
        assertEquals(result, expected, 0.01);
    }

    @Test
    public void getNumeroPintoresTesteTres() {
        // arrange
        double area = 1;
        double expected = 1;

        // act
        double result = ExercicioSete.getNumeroPintores(area);

        // assert
        assertEquals(result, expected, 0.01);
    }

    @Test
    public void getNumeroPintoresTesteQuatro() {
        // arrange
        double area = 100;
        double expected = 2;

        // act
        double result = ExercicioSete.getNumeroPintores(area);

        // assert
        assertEquals(result, expected, 0.01);
    }

    @Test
    public void getNumeroPintoresTesteCinco() {
        // arrange
        double area = 300;
        double expected = 3;

        // act
        double result = ExercicioSete.getNumeroPintores(area);

        // assert
        assertEquals(result, expected, 0.01);
    }

    @Test
    public void getNumeroPintoresTesteSeis() {
        // arrange
        double area = 1000;
        double expected = 4;

        // act
        double result = ExercicioSete.getNumeroPintores(area);

        // assert
        assertEquals(result, expected, 0.01);
    }

    @Test
    public void getCustoTintaTesteUm() {
        // arrange
        double area = 10;
        double custoTintaLitro = 10;
        double rendimentoLitro = 10;
        double expected = 10;

        // act
        double result = ExercicioSete.getCustoTinta(area, custoTintaLitro, rendimentoLitro);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getCustoTintaTesteDois() {
        // arrange
        double area = 10;
        double custoTintaLitro = 0;
        double rendimentoLitro = 10;
        double expected = 0;

        // act
        double result = ExercicioSete.getCustoTinta(area, custoTintaLitro, rendimentoLitro);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getCustoTintaTesteTres() {
        // arrange
        double area = 150;
        double custoTintaLitro = 9.99;
        double rendimentoLitro = 0.2;
        double expected = 7492.5;

        // act
        double result = ExercicioSete.getCustoTinta(area, custoTintaLitro, rendimentoLitro);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getCustoPintorTesteUm() {
        // arrange
        double area = 10;
        double salarioPintorDia = 10;
        double numeroPintores = 1;
        double expected = 10;

        // act
        double result = ExercicioSete.getCustoPintor(area, salarioPintorDia, numeroPintores);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getCustoPintorTesteDois() {
        // arrange
        double area = 0;
        double salarioPintorDia = 10;
        double numeroPintores = 1;
        double expected = 0;

        // act
        double result = ExercicioSete.getCustoPintor(area, salarioPintorDia, numeroPintores);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getCustoPintorTesteTres() {
        // arrange
        double area = 200;
        double salarioPintorDia = 10;
        double numeroPintores = 2;
        double expected = 70;

        // act
        double result = ExercicioSete.getCustoPintor(area, salarioPintorDia, numeroPintores);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getCustoTotalTesteUm() {
        // arrange
        double custoTinta = 10;
        double custoPintor = 10;
        double expected = 20;

        // act
        double result = ExercicioSete.getCustoTotal(custoTinta, custoPintor);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getCustoTotalTesteDois() {
        // arrange
        double custoTinta = 1;
        double custoPintor = 999;
        double expected = 1000;

        // act
        double result = ExercicioSete.getCustoTotal(custoTinta, custoPintor);

        // assert
        assertEquals(expected, result, 0.01);
    }
}