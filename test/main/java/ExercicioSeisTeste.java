package main.java;

import org.junit.Test;

import static org.junit.Assert.*;

public class ExercicioSeisTeste {

    @Test
    public void getHorasTesteUm() {
        // arrange
        int segundosTotal = 14401;
        double expected = 4;

        // act
        double result = ExercicioSeis.getHoras(segundosTotal);

        // assert
        assertEquals(result, expected, 0.01);
    }

    @Test
    public void getHorasTesteDois() {
        // arrange
        int segundosTotal = 0;
        double expected = 0;

        // act
        double result = ExercicioSeis.getHoras(segundosTotal);

        // assert
        assertEquals(result, expected, 0.01);
    }

    @Test
    public void getHorasTesteTres() {
        // arrange
        int segundosTotal = 82831;
        double expected = 23;

        // act
        double result = ExercicioSeis.getHoras(segundosTotal);

        // assert
        assertEquals(result, expected, 0.01);
    }

    @Test
    public void getMinutosTesteUm() {
        // arrange
        int segundosTotal = 14460;
        double expected = 1;

        // act
        double result = ExercicioSeis.getMinutos(segundosTotal);

        // assert
        assertEquals(result, expected, 0.01);
    }

    @Test
    public void getMinutosTesteDois() {
        // arrange
        int segundosTotal = 14400;
        double expected = 0;

        // act
        double result = ExercicioSeis.getMinutos(segundosTotal);

        // assert
        assertEquals(result, expected, 0.01);
    }

    @Test
    public void getMinutosTesteTres() {
        // arrange
        int segundosTotal = 82799;
        double expected = 59;

        // act
        double result = ExercicioSeis.getMinutos(segundosTotal);

        // assert
        assertEquals(result, expected, 0.01);
    }

    @Test
    public void getSegundosTesteUm() {
        // arrange
        int segundosTotal = 14401;
        double expected = 1;

        // act
        double result = ExercicioSeis.getSegundos(segundosTotal);

        // assert
        assertEquals(result, expected, 0.01);
    }

    @Test
    public void getSegundosTesteDois() {
        // arrange
        int segundosTotal = 14400;
        double expected = 0;

        // act
        double result = ExercicioSeis.getSegundos(segundosTotal);

        // assert
        assertEquals(result, expected, 0.01);
    }

    @Test
    public void getSegundosTesteTres() {
        // arrange
        int segundosTotal = 82859;
        double expected = 59;

        // act
        double result = ExercicioSeis.getSegundos(segundosTotal);

        // assert
        assertEquals(result, expected, 0.01);
    }

    @Test
    public void isDiaTardeNoiteTesteUm() {
        // arrange
        int horas = 6;
        int minutos = 0;
        int segundos = 0;
        String expected = "Bom dia";

        // act
        String result = ExercicioSeis.isDiaTardeNoite(horas, minutos, segundos);

        // assert
        assertEquals(expected, result);
    }

    @Test
    public void isDiaTardeNoiteTesteDois() {
        // arrange
        int horas = 12;
        int minutos = 0;
        int segundos = 1;
        String expected = "Boa tarde";

        // act
        String result = ExercicioSeis.isDiaTardeNoite(horas, minutos, segundos);

        // assert
        assertEquals(expected, result);
    }

    @Test
    public void isDiaTardeNoiteTesteTres() {
        // arrange
        int horas = 20;
        int minutos = 0;
        int segundos = 1;
        String expected = "Boa noite";

        // act
        String result = ExercicioSeis.isDiaTardeNoite(horas, minutos, segundos);

        // assert
        assertEquals(expected, result);
    }
}