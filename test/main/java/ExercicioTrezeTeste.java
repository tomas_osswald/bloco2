package main.java;

import org.junit.Test;

import static org.junit.Assert.*;

public class ExercicioTrezeTeste {

    @Test
    public void getInvalido() {
        // arrange
        double areaGrama = 1;
        double arvores = 1;
        double arbustos = -1;
        boolean expected = true;

        // act
        boolean result = ExercicioTreze.isInvalido(areaGrama, arvores, arbustos);

        // assert
        assertEquals(expected, result);
    }

    @Test
    public void getValido() {
        // arrange
        double areaGrama = 1;
        double arvores = 1;
        double arbustos = 1;
        boolean expected = false;

        // act
        boolean result = ExercicioTreze.isInvalido(areaGrama, arvores, arbustos);

        // assert
        assertEquals(expected, result);
    }

    @Test
    public void getHorasTotalTesteUm() {
        // arrange
        double areaGrama = 100;
        double arvores = 5;
        double arbustos = 5;
        double tempoGrama = 300;
        double tempoArvores = 600;
        double tempoArbustos = 400;
        double expected = 10;

        // act
        double result = ExercicioTreze.getHorasTotal(areaGrama, arvores, arbustos, tempoGrama, tempoArvores, tempoArbustos);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getHorasTotalTesteDois() {
        // arrange
        double areaGrama = 0;
        double arvores = 1;
        double arbustos = 0;
        double tempoGrama = 300;
        double tempoArvores = 600;
        double tempoArbustos = 400;
        double expected = 1;

        // act
        double result = ExercicioTreze.getHorasTotal(areaGrama, arvores, arbustos, tempoGrama, tempoArvores, tempoArbustos);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getHorasTotalTesteTres() {
        // arrange
        double areaGrama = 0;
        double arvores = 0;
        double arbustos = 0;
        double tempoGrama = 300;
        double tempoArvores = 600;
        double tempoArbustos = 400;
        double expected = 0;

        // act
        double result = ExercicioTreze.getHorasTotal(areaGrama, arvores, arbustos, tempoGrama, tempoArvores, tempoArbustos);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getCustoTotalTesteUm() {
        // arrange
        double areaGrama = 0;
        double arvores = 0;
        double arbustos = 0;
        double horasTotal = 0;
        double custoGrama = 10;
        double custoArvores = 20;
        double custoArbustos = 15;
        double custoTrabalhoHora = 10;
        double expected = 0;

        // act
        double result = ExercicioTreze.getCustoTotal(areaGrama, arvores, arbustos, horasTotal, custoGrama, custoArvores, custoArbustos, custoTrabalhoHora);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getCustoTotalTesteDois() {
        // arrange
        double areaGrama = 100;
        double arvores = 10;
        double arbustos = 10;
        double horasTotal = 10;
        double custoGrama = 10;
        double custoArvores = 20;
        double custoArbustos = 15;
        double custoTrabalhoHora = 10;
        double expected = 1450;

        // act
        double result = ExercicioTreze.getCustoTotal(areaGrama, arvores, arbustos, horasTotal, custoGrama, custoArvores, custoArbustos, custoTrabalhoHora);

        // assert
        assertEquals(expected, result, 0.01);
    }
}