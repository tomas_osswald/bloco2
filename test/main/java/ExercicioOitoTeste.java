package main.java;

import org.junit.Test;

import static org.junit.Assert.*;

public class ExercicioOitoTeste {

    @Test
    public void isMultiploTesteUm() {
        // arrange
        int numero1 = 4;
        int numero2 = 2;
        String expected = (numero1 + " é múltiplo de " + numero2);

        // act
        String result = ExercicioOito.isMultiplo(numero1, numero2);

        // assert
        assertEquals(expected, result);
    }

    @Test
    public void isMultiploTesteDois() {
        // arrange
        int numero1 = 5;
        int numero2 = 10;
        String expected = (numero2 + " é múltiplo de " + numero1);

        // act
        String result = ExercicioOito.isMultiplo(numero1, numero2);

        // assert
        assertEquals(expected, result);
    }

    @Test
    public void isMultiploTesteTres() {
        // arrange
        int numero1 = 31;
        int numero2 = 3;
        String expected = (numero1 + " não é múltiplo nem divisor de " + numero2);

        // act
        String result = ExercicioOito.isMultiplo(numero1, numero2);

        // assert
        assertEquals(expected, result);
    }

}