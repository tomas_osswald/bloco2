package main.java;

import org.junit.Test;

import static org.junit.Assert.*;

public class ExercicioDezasseisTeste {

    @Test
    public void isTrianguloPossivelTesteUm() {
        // arrange
        double anguloA = 60;
        double anguloB = 60;
        double anguloC = 60;
        boolean expected = true;

        // act
        boolean result = ExercicioDezasseis.isTrianguloPossivel(anguloA, anguloB, anguloC);

        // assert
        assertEquals(expected, result);
    }

    @Test
    public void isTrianguloPossivelTesteDois() {
        // arrange
        double anguloA = 59;
        double anguloB = 60;
        double anguloC = 60;
        boolean expected = false;

        // act
        boolean result = ExercicioDezasseis.isTrianguloPossivel(anguloA, anguloB, anguloC);

        // assert
        assertEquals(expected, result);
    }

    @Test
    public void isTrianguloPossivelTesteTres() {
        // arrange
        double anguloA = 180;
        double anguloB = 0;
        double anguloC = 0;
        boolean expected = false;

        // act
        boolean result = ExercicioDezasseis.isTrianguloPossivel(anguloA, anguloB, anguloC);

        // assert
        assertEquals(expected, result);
    }

    @Test
    public void isTrianguloPossivelTesteQuatro() {
        // arrange
        double anguloA = 181;
        double anguloB = -1;
        double anguloC = 0;
        boolean expected = false;

        // act
        boolean result = ExercicioDezasseis.isTrianguloPossivel(anguloA, anguloB, anguloC);

        // assert
        assertEquals(expected, result);
    }

    @Test
    public void isTrianguloRectanguloTesteUm() {
        // arrange
        double anguloA = 90;
        double anguloB = 45;
        double anguloC = 45;
        boolean expected = true;

        // act
        boolean result = ExercicioDezasseis.isTrianguloRectangulo(anguloA, anguloB, anguloC);

        // assert
        assertEquals(expected, result);
    }

    @Test
    public void isTrianguloRectanguloTesteDois() {
        // arrange
        double anguloA = 89;
        double anguloB = 46;
        double anguloC = 45;
        boolean expected = false;

        // act
        boolean result = ExercicioDezasseis.isTrianguloRectangulo(anguloA, anguloB, anguloC);

        // assert
        assertEquals(expected, result);
    }

    @Test
    public void isTrianguloObtusanguloTesteUm() {
        // arrange
        double anguloA = 91;
        double anguloB = 44;
        double anguloC = 45;
        boolean expected = true;

        // act
        boolean result = ExercicioDezasseis.isTrianguloObtusangulo(anguloA, anguloB, anguloC);

        // assert
        assertEquals(expected, result);
    }

    @Test
    public void isTrianguloObtusanguloTesteDois() {
        // arrange
        double anguloA = 89;
        double anguloB = 46;
        double anguloC = 45;
        boolean expected = false;

        // act
        boolean result = ExercicioDezasseis.isTrianguloObtusangulo(anguloA, anguloB, anguloC);

        // assert
        assertEquals(expected, result);
    }
}