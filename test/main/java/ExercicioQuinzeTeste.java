package main.java;

import org.junit.Test;

import static org.junit.Assert.*;

public class ExercicioQuinzeTeste {

    @Test
    public void isTrianguloImpossivelTesteUm() {
        // arrange
        double ladoA = 1;
        double ladoB = 1;
        double ladoC = 1;
        boolean expected = false;

        // act
        boolean result = ExercicioQuinze.isTrianguloImpossivel(ladoA, ladoB, ladoC);

        // assert
        assertEquals(result, expected);
    }

    @Test
    public void isTrianguloImpossivelTesteDois() {
        // arrange
        double ladoA = 0;
        double ladoB = 1;
        double ladoC = 1;
        boolean expected = true;

        // act
        boolean result = ExercicioQuinze.isTrianguloImpossivel(ladoA, ladoB, ladoC);

        // assert
        assertEquals(result, expected);
    }

    @Test
    public void isTrianguloImpossivelTesteTres() {
        // arrange
        double ladoA = -1;
        double ladoB = 1;
        double ladoC = 1;
        boolean expected = true;

        // act
        boolean result = ExercicioQuinze.isTrianguloImpossivel(ladoA, ladoB, ladoC);

        // assert
        assertEquals(result, expected);
    }

    @Test
    public void isTrianguloImpossivelTesteQuatro() {
        // arrange
        double ladoA = 4;
        double ladoB = 1;
        double ladoC = 1;
        boolean expected = true;

        // act
        boolean result = ExercicioQuinze.isTrianguloImpossivel(ladoA, ladoB, ladoC);

        // assert
        assertEquals(result, expected);
    }

    @Test
    public void isTrianguloEquilateroTesteUm() {
        // arrange
        double ladoA = 1;
        double ladoB = 1;
        double ladoC = 1;
        boolean expected = true;

        // act
        boolean result = ExercicioQuinze.isTrianguloEquilatero(ladoA, ladoB, ladoC);

        // assert
        assertEquals(result, expected);
    }

    @Test
    public void isTrianguloEquilateroTesteDois() {
        // arrange
        double ladoA = 2;
        double ladoB = 1;
        double ladoC = 1;
        boolean expected = false;

        // act
        boolean result = ExercicioQuinze.isTrianguloEquilatero(ladoA, ladoB, ladoC);

        // assert
        assertEquals(result, expected);
    }

    @Test
    public void isTrianguloEscalenoTesteUm() {
        // arrange
        double ladoA = 1;
        double ladoB = 2;
        double ladoC = 3;
        boolean expected = true;

        // act
        boolean result = ExercicioQuinze.isTrianguloEscaleno(ladoA, ladoB, ladoC);

        // assert
        assertEquals(result, expected);
    }

    @Test
    public void isTrianguloEscalenoTesteDois() {
        // arrange
        double ladoA = 2;
        double ladoB = 2;
        double ladoC = 3;
        boolean expected = false;

        // act
        boolean result = ExercicioQuinze.isTrianguloEscaleno(ladoA, ladoB, ladoC);

        // assert
        assertEquals(result, expected);
    }
}