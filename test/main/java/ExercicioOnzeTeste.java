package main.java;

import org.junit.Test;

import static org.junit.Assert.*;

public class ExercicioOnzeTeste {

    @Test
    public void getClassificacaoTesteUm() {
        // arrange
        double aprovados = 0.1;
        double limiteMin = 0;
        double limiteMax = 1;
        String expected = "Turma má";

        // act
        String result = ExercicioOnze.getClassificacao(aprovados, limiteMin, limiteMax);

        // assert
        assertEquals(expected, result);
    }

    @Test
    public void getClassificacaoTesteDois() {
        // arrange
        double aprovados = 0.3;
        double limiteMin = 0;
        double limiteMax = 1;
        String expected = "Turma fraca";

        // act
        String result = ExercicioOnze.getClassificacao(aprovados, limiteMin, limiteMax);

        // assert
        assertEquals(expected, result);
    }

    @Test
    public void getClassificacaoTesteTres() {
        // arrange
        double aprovados = 0.6;
        double limiteMin = 0;
        double limiteMax = 1;
        String expected = "Turma razoável";

        // act
        String result = ExercicioOnze.getClassificacao(aprovados, limiteMin, limiteMax);

        // assert
        assertEquals(expected, result);
    }

    @Test
    public void getClassificacaoTesteQuatro() {
        // arrange
        double aprovados = 0.8;
        double limiteMin = 0;
        double limiteMax = 1;
        String expected = "Turma boa";

        // act
        String result = ExercicioOnze.getClassificacao(aprovados, limiteMin, limiteMax);

        // assert
        assertEquals(expected, result);
    }

    @Test
    public void getClassificacaoTesteCinco() {
        // arrange
        double aprovados = 0.955;
        double limiteMin = 0;
        double limiteMax = 1;
        String expected = "Turma excelente";

        // act
        String result = ExercicioOnze.getClassificacao(aprovados, limiteMin, limiteMax);

        // assert
        assertEquals(expected, result);
    }

    @Test
    public void getClassificacaoTesteSeis() {
        // arrange
        double aprovados = 11;
        double limiteMin = 0;
        double limiteMax = 10;
        String expected = "Valor inválido";

        // act
        String result = ExercicioOnze.getClassificacao(aprovados, limiteMin, limiteMax);

        // assert
        assertEquals(expected, result);
    }
}