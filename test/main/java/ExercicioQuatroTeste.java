package main.java;

import org.junit.Test;

import static org.junit.Assert.*;

public class ExercicioQuatroTeste {

    @Test
    public void getResultadoTesteUm() {
        // arrange
        double variavel = 0;
        double expected = 0;

        // act
        double result = ExercicioQuatro.getResultado(variavel);

        // assert
        assertEquals(result, expected, 0.01);
    }

    @Test
    public void getResultadoTesteDois() {
        // arrange
        double variavel = -32;
        double expected = -32;

        // act
        double result = ExercicioQuatro.getResultado(variavel);

        // assert
        assertEquals(result, expected, 0.01);
    }

    @Test
    public void getResultadoTesteTres() {
        // arrange
        double variavel = 7.23;
        double expected = 37.81;

        // act
        double result = ExercicioQuatro.getResultado(variavel);

        // assert
        assertEquals(result, expected, 0.01);
    }
}