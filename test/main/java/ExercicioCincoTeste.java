package main.java;

import org.junit.Test;

import static org.junit.Assert.*;

public class ExercicioCincoTeste {

    @Test
    public void getVolumeTesteUm() {
        // arrange
        double area = 50;
        double expected = 24.06;

        // act
        double result = ExercicioCinco.getVolume(area);

        // assert
        assertEquals(result, expected, 0.01);
    }

    @Test
    public void getVolumeTesteDois() {
        // arrange
        double area = 0;
        double expected = 0;

        // act
        double result = ExercicioCinco.getVolume(area);

        // assert
        assertEquals(result, expected, 0.01);
    }

    @Test
    public void getVolumeTesteTres() {
        // arrange
        double area = 937.4;
        double expected = 1952.81;

        // act
        double result = ExercicioCinco.getVolume(area);

        // assert
        assertEquals(result, expected, 0.01);
    }

    @Test
    public void isTamanhoTesteUm() {
        // arrange
        double volume = 1000;
        String expected = "O cubo é pequeno";

        // act
        String result = ExercicioCinco.isTamanho(volume);

        // assert
        assertEquals(result, expected);
    }

    @Test
    public void isTamanhoTesteDois() {
        // arrange
        double volume = 2000;
        String expected = "O cubo é médio";

        // act
        String result = ExercicioCinco.isTamanho(volume);

        // assert
        assertEquals(result, expected);
    }

    @Test
    public void isTamanhoTesteTres() {
        // arrange
        double volume = 2001;
        String expected = "O cubo é grande";

        // act
        String result = ExercicioCinco.isTamanho(volume);

        // assert
        assertEquals(result, expected);
    }

    @Test
    public void isValidoTesteUm() {
        // arrange
        double area = 1;
        double expected = 1;

        // act
        double result = ExercicioCinco.isValido(area);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void isValidoTesteDois() {
        // arrange
        double area = 0;
        double expected = 0;

        // act
        double result = ExercicioCinco.isValido(area);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void isValidoTesteTres() {
        // arrange
        double area = -1;
        double expected = 0;

        // act
        double result = ExercicioCinco.isValido(area);

        // assert
        assertEquals(expected, result, 0.01);
    }
}