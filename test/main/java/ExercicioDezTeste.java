package main.java;

import org.junit.Test;

import static org.junit.Assert.*;

public class ExercicioDezTeste {

    @Test
    public void isValidoTesteUm() {
        // arrange
        double precoBase = 1;
        int expected = 1;

        // act
        int result = ExercicioDez.isValido(precoBase);

        // assert
        assertEquals(result, expected, 0.01);
    }

    @Test
    public void isValidoTesteDois() {
        // arrange
        double precoBase = 0;
        int expected = 0;

        // act
        int result = ExercicioDez.isValido(precoBase);

        // assert
        assertEquals(result, expected, 0.01);
    }

    @Test
    public void isValidoTesteTres() {
        // arrange
        double precoBase = -1;
        int expected = 0;

        // act
        int result = ExercicioDez.isValido(precoBase);

        // assert
        assertEquals(result, expected, 0.01);
    }

    @Test
    public void getDescontoTesteUm() {
        // arrange
        double precoBase = 50;
        double expected = 0.2;

        // act
        double result = ExercicioDez.getDesconto(precoBase);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getDescontoTesteDois() {
        // arrange
        double precoBase = 100;
        double expected = 0.3;

        // act
        double result = ExercicioDez.getDesconto(precoBase);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getDescontoTesteTres() {
        // arrange
        double precoBase = 200;
        double expected = 0.4;

        // act
        double result = ExercicioDez.getDesconto(precoBase);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getDescontoTesteQuatro() {
        // arrange
        double precoBase = 201;
        double expected = 0.6;

        // act
        double result = ExercicioDez.getDesconto(precoBase);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getPrecoFinalTesteUm() {
        // arrange
        double precoBase = 50;
        double desconto = 0.2;
        double expected = 40;

        // act
        double result = ExercicioDez.getPrecoFinal(precoBase, desconto);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getPrecoFinalTesteDois() {
        // arrange
        double precoBase = 299.99;
        double desconto = 0.6;
        double expected = 120.00;

        // act
        double result = ExercicioDez.getPrecoFinal(precoBase, desconto);

        // assert
        assertEquals(expected, result, 0.01);
    }
}