package main.java;

import org.junit.Test;

import static org.junit.Assert.*;

public class ExercicioVinteTeste {

    @Test
    public void getValorAluguerTesteUm() {
        // arrange
        int tipoDeKit = 1;
        int diaDaSemana = 1;
        double expected = 30;

        // act
        double result = ExercicioVinte.getValorAluguer(tipoDeKit, diaDaSemana);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getValorAluguerTesteDois() {
        // arrange
        int tipoDeKit = 1;
        int diaDaSemana = 2;
        double expected = 40;

        // act
        double result = ExercicioVinte.getValorAluguer(tipoDeKit, diaDaSemana);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getValorAluguerTesteTres() {
        // arrange
        int tipoDeKit = 2;
        int diaDaSemana = 1;
        double expected = 50;

        // act
        double result = ExercicioVinte.getValorAluguer(tipoDeKit, diaDaSemana);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getValorAluguerTesteQuatro() {
        // arrange
        int tipoDeKit = 2;
        int diaDaSemana = 2;
        double expected = 70;

        // act
        double result = ExercicioVinte.getValorAluguer(tipoDeKit, diaDaSemana);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getValorAluguerTesteCinco() {
        // arrange
        int tipoDeKit = 3;
        int diaDaSemana = 1;
        double expected = 100;

        // act
        double result = ExercicioVinte.getValorAluguer(tipoDeKit, diaDaSemana);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getValorAluguerTesteSeis() {
        // arrange
        int tipoDeKit = 3;
        int diaDaSemana = 2;
        double expected = 140;

        // act
        double result = ExercicioVinte.getValorAluguer(tipoDeKit, diaDaSemana);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getValorTransporteTesteUm() {
        // arrange
        double distancia = 1;
        double expected = 2;

        // act
        double result = ExercicioVinte.getValorTransporte(distancia);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getValorTransporteTesteDois() {
        // arrange
        double distancia = 0;
        double expected = 0;

        // act
        double result = ExercicioVinte.getValorTransporte(distancia);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getValorTransporteTesteTres() {
        // arrange
        double distancia = 31;
        double expected = 62;

        // act
        double result = ExercicioVinte.getValorTransporte(distancia);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getValorTotalTesteUm() {
        // arrange
        double valorTransporte = 1;
        double valorAluguer = 1;
        double expected = 2;

        // act
        double result = ExercicioVinte.getValorTotal(valorAluguer, valorTransporte);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getValorTotalTesteDois() {
        // arrange
        double valorTransporte = 0;
        double valorAluguer = 0;
        double expected = 0;

        // act
        double result = ExercicioVinte.getValorTotal(valorAluguer, valorTransporte);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getValorTotalTesteTres() {
        // arrange
        double valorTransporte = 37;
        double valorAluguer = 41;
        double expected = 78;

        // act
        double result = ExercicioVinte.getValorTotal(valorAluguer, valorTransporte);

        // assert
        assertEquals(expected, result, 0.01);
    }
}